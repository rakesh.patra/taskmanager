# Task manager App

This app helps managing todo task list for a manager.
Follow the steps to set-up your application.
* Install ruby 2.3.4
* Intall mysql
* Install gems using bundler
run ``` bundle install ```
* Add database configuration by copying config/database.yml.example
run ```rake db:create``` and then ```rake db:migrate```

* Add default manager and developers to db.
run ```rake data:migrate```
* start rails server using ```rails s```
* Note: Manager can create/edit projects and tasks.
 Developer can only update task status.
