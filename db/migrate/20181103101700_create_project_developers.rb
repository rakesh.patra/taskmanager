class CreateProjectDevelopers < ActiveRecord::Migration[5.2]
  def change
    create_table :project_developers do |t|
      t.references :project, null: false
      t.references :user, null: false
      t.timestamps
    end
  end
end
