class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :name, null: false
      t.string :description, default: ""
      t.references :project, null: false
      t.integer :user_id
      t.string :status, null: false, default: "new"
      t.timestamps
    end

    add_index :tasks, :user_id
  end
end
