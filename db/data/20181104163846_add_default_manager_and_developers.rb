class AddDefaultManagerAndDevelopers < ActiveRecord::Migration[5.2]
  def up
    users =
    [
      {name: "Manager1", email: 'manager1@todoapp.com', user_type: "manager", password: 'manager1@todoapp'},
      {name: "Dev1", email: 'dev1@todoapp.com', user_type: "developer", password: 'developer1@todoapp'},
      {name: "Dev2", email: 'dev2@todoapp.com', user_type: "developer", password: 'developer2@todoapp'},
      {name: "Dev3", email: 'dev3@todoapp.com', user_type: "developer", password: 'developer3@todoapp'},
      {name: "Dev4", email: 'dev4@todoapp.com', user_type: "developer", password: 'developer4@todoapp'}
    ]
    users.each{|user| User.create(user)}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
