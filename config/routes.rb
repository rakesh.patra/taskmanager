Rails.application.routes.draw do
  root 'dashboards#index'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :projects, only: [:new, :create] do
    get :stats, on: :collection
  end
  resources :tasks, only: [:new, :create, :index ,:edit, :update]
end
