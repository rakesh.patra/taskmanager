class TasksController < ApplicationController
  before_action :set_task, only: [:edit, :update]

  def index
    authorize :task

    @tasks = TaskPolicy::Scope.new(pundit_user, "Task").resolve
  end

  def new
    authorize :task

    @task = Task.new
    @developers = User.developers
    @projects = Project.all
  end

  def create
    authorize :task

    task = Task.new(task_params.merge!(status: 'new'))
    if task.save
      redirect_to tasks_path, notice: "Task Created successfully."
    else
      @developers = User.developers
      @projects = Project.all
      flash.now[:error] = "Unable to create task."
      render :new
    end
  end

  def edit
    authorize @task

    @developers = User.developers
    @projects = Project.all
  end

  def update
    authorize @task
    if @task.update_attributes(task_params)
      redirect_to tasks_path, notice: "Task updated successfully."
    else
      @developers = User.developers
      @projects = Project.all
      flash.now[:error] = "Unable to update task."
      render :new
    end
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(policy(Task).permitted_attributes)
  end
end
