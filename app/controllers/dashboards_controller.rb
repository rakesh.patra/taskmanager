class DashboardsController < ApplicationController
  def index
    redirect_to tasks_path unless current_user.manager?

    tasks = Task.select('tasks.name as name, tasks.id as id,
                         tasks.status,
                         projects.name as project_name,
                         projects.id as project_id,
                         users.id as user_id, users.name as user_name')
                .joins(:user, :project)
    @status_hash = {done: "Done", in_progress: "In progress", new: "New"}
    @developer_tasks = {}
    @project_tasks = {}
    tasks.each do |task|
      @developer_tasks[task.user_id] = {"done" => [], "in_progress" => [], "new" => [], "name" => task.user_name} unless @developer_tasks[task.user_id]
      @developer_tasks[task.user_id][task.status] << task
      @project_tasks[task.project_id] = {"done" => [], "in_progress" => [], "new" => [], "name" => task.project_name} unless @project_tasks[task.project_id]
      @project_tasks[task.project_id][task.status] << task
    end
  end
end
