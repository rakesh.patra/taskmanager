class ProjectsController < ApplicationController
  def new
    authorize :project

    @project = Project.new
    @developers = User.developers
  end

  def create
    authorize :project

    @project = Project.new(project_params.merge!(manager_id: current_user.id))
    if @project.save
      redirect_to '/', notice: "Project Created successfully."
    else
      @developers = User.developers
      flash.now[:error] = "Unable to create project."
      render :new
    end
  end

  def stats
    stats = Project.select('COUNT(tasks.id) as count, tasks.status').joins(:tasks).group('tasks.status')
    stats_array = stats.map{|stat| [stat.status.humanize, stat.count]}

    render json: stats_array
  end

  private

  def project_params
    params.require(:project).permit(:name, :user_ids)
  end
end
