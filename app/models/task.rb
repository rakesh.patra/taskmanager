class Task < ApplicationRecord
  belongs_to :project
  belongs_to :user, optional: true
  validates_presence_of :name
  validates_presence_of :project_id

  scope :with_status, -> (status) { where(status: status) }
end
