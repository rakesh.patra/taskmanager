class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :projects, foreign_key: 'manager_id'
  has_many :tasks
  has_many :users, through: :projects

  alias_attribute :project_developers, :users

  scope :developers, -> { where(user_type: "developer") }

  ['manager', 'developer'].each do |user_type|

    method_name = "#{user_type}?"
    define_method(method_name.to_sym) do
      self.user_type == user_type
    end
  end
end
