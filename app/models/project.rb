class Project < ApplicationRecord
  has_many :tasks
  belongs_to :manager, class_name: "User"
  has_many :project_developers
  has_many :users, through: :project_developers

  validates_presence_of :name
end
