function drawChart(chartData = null) {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Status');
  data.addColumn('number', 'percentage');
  // chartData = [['done', 1], ['new', 5]]

  data.addRows(chartData);

  // Set chart options
  var options = {'title':'Project status',
                 'width':400,
                 'height':300};

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
  chart.draw(data, options);
};

function loadChart(){
  $.ajax({
    url: "/projects/stats",
    type: 'GET',
    dataType: "json",
    success: function(data){
      drawChart(data);
    },
    failure: function(){
      console.log('Error');
    }
  });
}