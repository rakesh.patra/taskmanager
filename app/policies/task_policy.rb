class TaskPolicy < ApplicationPolicy
  def update?
    user.manager? || (user.developer? and record.user_id == user.id)
  end

  def edit?
    update?
  end

  def index?
    user.manager? || user.developer?
  end

  def permitted_attributes
    return [:name, :description, :project_id, :user_id, :status] if user.manager?
    return [:status]
  end

  class Scope < Scope
    def resolve
      send "resolve_for_#{user.user_type}"
    rescue
      []
    end

    def resolve_for_manager
      Task.joins(:project).where(projects: {manager_id: user.id})
    end

    def resolve_for_developer
      Task.where(user_id: user.id)
    end
  end
end